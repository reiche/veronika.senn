import tkinter as tk
from tkinter import ttk
from src.AnalysisDisplayTabs import Even_Odd_Dist_Tab, Freq_Ana_Tab, RMS_And_First_EV_Tab, Rel_Magn_Tab, EV_Detraced_RMS_Tab
from src.InputFormStruc import InputForm
from src.GetBPMData import generate_data
from src.HDF5filingStruc import HDF5_file
from src.PCAStruc import PCA


def chn_no_output(BPMDat, chn_name):
    #Function to scan the channel name list for the desired BPM channel index. If not found a default channel is used instead
    if BPMDat.line=='Aramis':
        chn_no = int(92)
    else:
        chn_no = int(93)
    if not chn_name == "":
        for i, chn in enumerate(BPMDat.chn_labels):
            if chn == chn_name:
                chn_no = i
    return chn_no


def _quit():
    #tk function to stop the loop and collapse the window.
    root.quit()
    root.destroy()



#User input
input = InputForm()
if input.save_path=="" and input.file_path=="":
    if input.new_data:
        input.save_path = 'Data/'
    else:
        input.file_path = 'Data/'

if input.new_data or not input.file_name == "":
    #Data collection
    if input.new_data:
        if input.no_shots=="":
            file = generate_data(input.save_path)
        else:
            file = generate_data(input.save_path, shots=input.no_shots)
    else:
        file = HDF5_file(input.file_name, input.file_path)
    AR_Data, AT_Data = file.file_readout()

    #Data display according to user input
    root = tk.Tk()
    root.title('Analysis Output')
    tabControl = ttk.Notebook(root)
    if input.odd_even_distribution:
        AR_chn_no = chn_no_output(AR_Data, input.ar_chn)
        AT_chn_no = chn_no_output(AT_Data, input.at_chn)
        tab1 = Even_Odd_Dist_Tab(tabControl, AR_Data, AT_Data, 0, AR_chn_no, AT_chn_no)
        tabControl.add(tab1, text='Even-Odd Distribution')
    if input.frequency_analysis:
        tab2 = Freq_Ana_Tab(tabControl, AR_Data, AT_Data)
        tabControl.add(tab2, text = 'Frequency Analysis')
    if input.rms_first_ev or input.relative_magnitudes or input.ev_detracted_rms:
        ARpca = PCA(AR_Data)
        ATpca= PCA(AT_Data)
        if input.rms_first_ev:
            tab3 = RMS_And_First_EV_Tab(tabControl, ARpca, ATpca)
            tabControl.add(tab3, text='RMS and First EV')
        if input.relative_magnitudes:
            tab4 = Rel_Magn_Tab(tabControl, ARpca, ATpca)
            tabControl.add(tab4, text='Relative Magnitudes')
        if input.ev_detracted_rms:
            tab5 = EV_Detraced_RMS_Tab(tabControl, ARpca, ATpca, input.resolution)
            tabControl.add(tab5, text='Reduced RMS by detracted EVs')
    tabControl.pack(expand=1, fill='both')

    button = tk.Button(master=root, text="Quit", command=_quit)
    button.pack(side=tk.BOTTOM)

    root.mainloop()
