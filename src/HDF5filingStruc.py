import h5py
import numpy as np
from src.DataStruc import BPMData

'''
    Class used for saving objects of the class BPMData to HDF5 files or reading out BPMData-HDF5 files to output BPMData objects 
    Important Functions:
        save_BPMData(filename, Mat1, Mat2, labels=np.array([0.0]))
        file_readout(axis=0)
'''

class HDF5_file:
    def __init__(self, filename, path=""):
        self.path = path        #String containing the path to the desired directory
        self.name = filename    #Sting containing the filename

    def save_BPMData(self, Mat1, Mat2, labels=np.array([0.0])):
        '''
        Produces a hdf5 file containing all the BPMData-information
        :param Mat1: Object of the class BPMData from the Aramis line
        :param Mat2: Object of the class BPMData from the Aramis line
        :param labels: Numpy array containing the pulse-ids of the samples contained in the BPMData inputs
        :return: -
        '''
        utf8_type = h5py.string_dtype('utf-8', 20)
        if self.path == "":
            hf = h5py.File(self.name, 'w')
        else:
            hf = h5py.File(self.path+self.name, 'w')
        g1 = hf.create_group('Data_Group')
        g2 = hf.create_group('Characterization_Group')
        g1.create_dataset('X1_Axis', data=Mat1.xmat)
        g1.create_dataset('X2_Axis', data=Mat2.xmat)
        g1.create_dataset('Y1_Axis', data=Mat1.ymat)
        g1.create_dataset('Y2_Axis', data=Mat2.ymat)
        g2.create_dataset('Pulse_ID', data=labels)
        pg = g2.create_group('Z_Axis_Positions')
        pg.create_dataset('X1_Positions', data=Mat1.pos)
        pg.create_dataset('X2_Positions', data=Mat2.pos)
        lg = g2.create_group('Channel_Names')
        chn1 = np.array([channel.encode("utf-8") for channel in Mat1.chn_labels], dtype=utf8_type)
        chn2 = np.array([channel.encode("utf-8") for channel in Mat2.chn_labels], dtype=utf8_type)
        lg.create_dataset('Channellist_Axis_1', data=chn1)
        lg.create_dataset('Channellist_Axis_2', data=chn2)
        hf.close()

    def generate_by_h5(self, axis):
        '''
        Generates and outputs an object of class BPMData from the hdf5-file.
        :param axis: Needs to be either 1 or 2. Depending on the index value, the Aramis or Athos line get set in for BPMData.line
        :return: Object of class BPMData
        '''
        Data = BPMData()
        Data.label = self.name[12:-3]
        if self.path=="":
            filename = self.name
        else:
            filename = self.path+self.name
        f = h5py.File(filename, 'r')
        if axis == 1 or axis == 2:
            Data.xmat = np.array(f.get('Data_Group/X'+str(axis)+'_Axis'))
            Data.ymat = np.array(f.get('Data_Group/Y'+str(axis)+'_Axis'))
            Data.pos = np.array(f.get('Characterization_Group/Z_Axis_Positions/X'+str(axis)+'_Positions'))
            Data.chn_labels = np.array([c.decode("utf-8") for c in f.get('Characterization_Group/Channel_Names/Channellist_Axis_'+str(axis))])
            if axis == 1:
                Data.line = 'Aramis'
            else:
                Data.line = 'Athos'
            return Data
        else:
            print('Faulty Axis Input')
            return

    def file_readout(self, axis=0):
        '''
        Outputs generated BPMData objects according to the distinction cases made by input parameter "axis"
        :param axis:Must be either 1, 2 or any. Input 1 indicates that an Aramis BPMData object will be output, input 2 indicates that an Aramis BPMData object will be output and any input will output both BPMData objects
        :return: Object(s) of class BPMData
        '''
        if axis == 1:
            return self.generate_by_h5(1)
        elif axis == 2:
            return self.generate_by_h5(2)
        else:
            Data1 = self.generate_by_h5(1)
            Data2 = self.generate_by_h5(2)
            return Data1, Data2
