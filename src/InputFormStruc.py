from tkinter import *
from tkinter import ttk

'''
    Class for collecting the input parameters used to display the desired analysis plots. 
    Initializing an object of this class causes an input form structured through tkinter to appear. The input will overwrite the current instance by pressing "submit". 
    The input form is structured in three subframes arranged in a grid. The widgets within the subframes are themselves arranged in a grid. 
'''


class InputForm():
    def __init__ (self):
        self.new_data = False
        self.no_shots = ""
        self.save_path = ""
        self.file_path = ""
        self.file_name = ""
        self.odd_even_distribution = False
        self.ar_chn = ""
        self.at_chn = ""
        self.frequency_analysis = False
        self.rms_first_ev = False
        self.rms_first_ev_save = False
        self.relative_magnitudes = False
        self.ev_detracted_rms = False
        self.resolution = 1

        options = [x for x in range(0,7)]

        #Initialise Parent Window
        root = Tk()
        root.title('Input Form')

        #Intitialise Variables
        new_dat = BooleanVar()
        oed = BooleanVar()
        freq = BooleanVar()
        ev = BooleanVar()
        relmag = BooleanVar()
        rms = BooleanVar()
        no_of_ev = IntVar()
        no_of_ev.set(1)

        #---------------------First Frame------------------------------------------------------
        frm = ttk.Frame(root, padding=30)
        frm.grid()
        frm1 = ttk.Frame(frm, relief=GROOVE, borderwidth=2, padding=10)
        frm1.grid(row=0, column=0)
        frm1.grid()

        ttk.Label(frm1, text="Data Set Used for Analysis:", font=('calibri', '11', 'bold')).grid(column=0, row=0, columnspan=5)
        ttk.Label(frm1, text='').grid(row=1, column=0)
        ttk.Label(frm1, text='').grid(row=2, column=0)
        ttk.Label(frm1, text="   Create new data?   ").grid(row=18, column=0, padx=75)

        ttk.Radiobutton(frm1, text='Yes', variable=new_dat, value=1).grid(row=10, column=1)
        ttk.Label(frm1, text='Desired amount of samples   ').grid(row=11, column=2, sticky='w')
        no_of_shots = ttk.Entry(frm1, width=30, font=("calibri", 9))
        no_of_shots.grid(row=11, column=3, sticky='w')
        ttk.Label(frm1, text='(Default is set to 5000 shots)'). grid(row=11, column=4, sticky='w')
        ttk.Label(frm1, text='Path to save directory').grid(row=13, column=2, sticky='w')
        savepath = ttk.Entry(frm1, width=30, font=("calibri", 9))
        savepath.grid(row=13, column=3, sticky='w')
        ttk.Label(frm1, text='(Default is set to Data directory)').grid(row=13, column=4, sticky='w')
        ttk.Label(frm1, text='').grid(row=19, column=0)

        ttk.Radiobutton(frm1, text='No', variable=new_dat, value=0).grid(row=20, column=1)
        ttk.Label(frm1, text='File name*').grid(row=21, column=2, sticky='w')
        filename = ttk.Entry(frm1, width=30,  font=("calibri", 9))
        filename.grid(row=21, column=3, sticky='w')
        ttk.Label(frm1, text='(expl: BPMReadouts_20200101_120000.h5 )').grid(row=21, column=4, sticky='w')
        ttk.Label(frm1, text='File path').grid(row=22, column=2, sticky='w')
        filepath = ttk.Entry(frm1, width=30,  font=("calibri", 9))
        filepath.grid(row=22, column=3, sticky='w')
        ttk.Label(frm1, text='(expl: /afs/psi.ch/user/m/muster_m/measurements/ ').grid(row=22, column=4, sticky='w')
        ttk.Label(frm1, text='default is set to Data directory )').grid(row=23, column=4, sticky='w')
        ttk.Label(frm1, text='').grid(row=30, column=0)

        ttk.Label(frm, text=' ').grid(row=1, column=0)

        #---------------------Second Frame--------------------------------
        frm2 = ttk.Frame(frm, relief=GROOVE, padding=10)
        frm2.grid(row=2, column=0)
        frm2.grid()
        ttk.Label(frm2, text='Desired Analysis Output: ', font=('calibri', '11', 'bold')).grid(row=11, column=0, columnspan=4)
        ttk.Label(frm2, text=' ').grid(row=12, column=0)

        ttk.Label(frm2, text=' ').grid(row=30, column=0)
        ttk.Label(frm2, text='Development of 50Hz Frequency Amplitude   ').grid(row=31, column=0, sticky='w')
        ttk.Checkbutton(frm2, variable=freq, onvalue=1, offvalue=0).grid(row=31, column=1)

        ttk.Label(frm2, text=' ').grid(row=40, column=0)
        ttk.Label(frm2, text='RMS and first eigenvector ').grid(row=41, column=0, sticky='w')
        ttk.Checkbutton(frm2, variable=ev, onvalue=1, offvalue=0).grid(row=41, column=1)

        ttk.Label(frm2, text=' ').grid(row=50, column=0)
        ttk.Label(frm2, text='Relative Magnitudes ').grid(row=51, column=0, sticky='w')
        ttk.Checkbutton(frm2, variable=relmag, onvalue=1, offvalue=0).grid(row=51, column=1)

        ttk.Label(frm2, text=' ').grid(row=60, column=0)
        ttk.Label(frm2, text='EV detracted RMS ').grid(row=61, column=0, sticky='w')
        ttk.Checkbutton(frm2, variable=rms, onvalue=1, offvalue=0).grid(row=61, column=1)
        ttk.Label(frm2, text='   Number of eivenvectors to detract').grid(row=61, column=2, sticky='e')
        ttk.OptionMenu(frm2, no_of_ev, *options, style = 'raised.TMenubutton').grid(row=61, column=3, sticky='w')

        ttk.Label(frm2, text=' ').grid(row=70, column=0)
        ttk.Label(frm2, text="Even-Odd Distribution Slices").grid(row=71, column=0, sticky='w')
        ttk.Checkbutton(frm2, variable=oed, onvalue=1, offvalue=0).grid(row=71, column=1)
        ttk.Label(frm2, text='Aramis channel ').grid(row=71, column=2, sticky='e')
        ar_c = ttk.Entry(frm2, width=25,  font=("calibri", 9))
        ar_c.grid(row=71, column=3)
        ttk.Label(frm2, text='Athos channel ').grid(row=72, column=2, sticky='e')
        at_c = ttk.Entry(frm2, width=25,  font=("calibri", 9))
        at_c.grid(row=72, column=3)

        ttk.Label(frm, text=' ').grid(row=3, column=0)

        #------------------Third Frame----------------------------------------
        def ok():
            self.new_data = new_dat.get()
            self.save_path = savepath.get()
            self.no_shots = no_of_shots.get()
            self.file_path = filepath.get()
            self.file_name = filename.get()
            self.odd_even_distribution = oed.get()
            self.ar_chn = ar_c.get()
            self.at_chn = at_c.get()
            self.frequency_analysis = freq.get()
            self.rms_first_ev = ev.get()
            self.relative_magnitudes = relmag.get()
            self.ev_detracted_rms = rms.get()
            self.resolution = no_of_ev.get()
            root.destroy()

        frm3 = ttk.Frame(frm, padding=10)
        frm3.grid(row=4, column=0)
        frm3.grid()
        ttk.Label(frm3, text=' ').grid(row=0, column=0)
        ttk.Label(frm3, text='Input fields marked with * need to be filled out if the correspondig option has been chosen').grid(row=10, column=0)
        ttk.Button(frm3, text="Submit", command=ok).grid(row=11, column=0, ipady=5, ipadx=10)

        root.mainloop()
