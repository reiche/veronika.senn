from epics import caget
from bsread import dispatcher, source
import numpy as np

'''
    Functions to gather sorted lists of currently available BPM channels for the x&y-axis on the Aramis (X1, Y1) and Athos (X2, Y2) lines respectively. 
    Main function: AssembleList()
'''


def remove_invalids(dictionary, channellist_x, channellist_y, z_axis):
    '''
    function to check the 'channel-VALID' values and delete channels from the lists accordingly
    :param dictionary: Dictionary containing the 'channel-VALID' values
    :param channellist_x: List with the sorted x-axis channels
    :param channellist_y: List with the sorted y-axis channels
    :param z_axis: List with the positions corresponding to the x&y-channels
    :return: adjusted x-axis list, adjusted y-axis list, adjusted position list (all numpy arrays)
    '''
    list_of_indices = np.array([], dtype=int)
    pos = np.copy(z_axis)
    for i, channel in enumerate(channellist_x):
        if dictionary.data.data[channel+'-VALID'].value == 0:
            list_of_indices = np.append(list_of_indices, i)
    if not len(list_of_indices) == 0:
        channellist_x = np.delete(channellist_x, list_of_indices)
        channellist_y = np.delete(channellist_y, list_of_indices)
        pos = np.delete(pos, list_of_indices)
    return channellist_x, channellist_y, pos

def AssembleList():
    '''
    Program outputs the currently available BPM channels on the x&y- axis on the Aramis and Athos line sorted by the z coordinate
    :return: numpy array of the sorted X1 and Y1 BPM channels, numpy array of the sorted X2 and Y2 BPM channels, numpy array of the positions of the X1/Y1 channels, numpy array of the positions of the X2/Y2 channels
    '''

    #Collecting all of the channels and sort out the BPM channels into X1, Y1, X2, Y2 lists
    channels = dispatcher.get_current_channels()
    all_channels = np.array([x['name'] for x in channels])
    lst_x1 = np.array([], dtype=str)
    lst_x2 = np.array([], dtype=str)
    lst_y1 = np.array([], dtype=str)
    lst_y2 = np.array([], dtype=str)
    for chl in all_channels:
        if chl[9:12] == 'BPM' and chl[19:24] == 'VALID':
            if chl[16:18] == 'X1':
                lst_x1 = np.append(lst_x1, chl[:18])
            elif chl[16:18] == 'X2':
                lst_x2 = np.append(lst_x2, chl[:18])
            elif chl[16:18] == 'Y1':
                lst_y1 = np.append(lst_y1, chl[:18])
            elif chl[16:18] == 'Y2':
                lst_y2 = np.append(lst_y2, chl[:18])
            else:
                continue

    #Compiling a dictionary of positions corresponding to the channels
    BPMpos = {}
    for chn in lst_x1:
        dev = chn.split(':')[0]
        if not dev in BPMpos.keys():
            BPMpos[dev] = caget(dev+':Z-POS')

    #Sorting each list according to the z-coordinates of the channels
    copy_list = np.copy(lst_x1)
    positions = np.zeros(len(lst_x1))
    for i, chn in enumerate(lst_x1):
        positions[i] = BPMpos[chn.split(':')[0]]
    ind = np.argsort(positions)
    for i, n in enumerate(ind):
        lst_x1[i] = copy_list[n]
        lst_y1[i] = copy_list[n][:16]+'Y1'
        lst_x2[i] = copy_list[n][:16]+'X2'
        lst_y2[i] = copy_list[n][:16]+'Y2'
    positions = np.sort(positions)

    #Sorting out AR or AT channels from the corresponding Athos or Aramis channel lists after the seperation point
    index_BS = 0
    while positions[index_BS] < 275.00:
        index_BS += 1
    index_list_1 = np.array([], dtype=int)
    index_list_2 = np.array([], dtype=int)
    for k in range(index_BS+1,len(lst_x1)):
        if lst_x1[k][1:3] == 'AT':
            index_list_1 = np.append(index_list_1, k)
        elif lst_x1[k][1:3] == 'AR' or lst_x1[k][1:3] == '30':
            index_list_2 = np.append(index_list_2, k)
    chlst_x1 = np.delete(lst_x1, index_list_1)
    chlst_y1 = np.delete(lst_y1, index_list_1)
    pos1 = np.delete(positions, index_list_1)
    chlst_x2 = np.delete(lst_x2, index_list_2)
    chlst_y2 = np.delete(lst_y2, index_list_2)
    pos2 = np.delete(positions, index_list_2)

    #Removing currently unavailable channels from the list by collecting 'channel-VALID' values from the stream
    first_and_second_axis = np.concatenate((chlst_x1, chlst_x2))
    val_chlst = np.array([chn+'-VALID' for chn in first_and_second_axis])

    with source(channels=val_chlst) as stream:
        val_msg = stream.receive()

    x1_axis, y1_axis, positions_1 = remove_invalids(val_msg, chlst_x1, chlst_y1, pos1)
    x2_axis, y2_axis, positions_2 = remove_invalids(val_msg, chlst_x2, chlst_y2, pos2)

    return np.array([x1_axis, y1_axis]), np.array([x2_axis, y2_axis]), positions_1, positions_2



