import numpy as np
import matplotlib.pyplot as plt

'''
    Class that stuctures the raw BPM data. The main BPM data is stored in the x- and y-matrices, while additional information associated is provided in the z-positions, axis, channel-labels and the date/time label.
    Class functions can be grouped in data information and 3 basic analysis types: statistics (mean, rms), Fourier spectrum and even-odd distribution. 
        Data Information        amount_shots()
                                amount_channels()
        Statistics:             mean()
                                rms()
        Fourier Spectrum:       freq_spec(axis,chn)             
                                get_frequencies()
                                freq_norm_factors(axis)
                                freq_development(axis,freq)
        Even-Odd Distribution:  odd_even_output(chn)
        
    Class functions to create single plots: display_rms(output_channels = 'all', save=False)
                                            display_freq_development(axis, freq, save=False)
                                            display_odd_even_distribution(chn, save=False)
                                            
'''

class BPMData:
    def __init__(self, x_Matrix=np.array([[0.0,0.0],[0.0,0.0]]), y_Matrix=np.array([[0.0,0.0],[0.0,0.0]]), z_Position=np.array([0.0,0.0]), axis='Not Determined', channel_labels=np.array(['none']), lab='none'):
        self.xmat = x_Matrix                #A numpy array matrix containing the data points on the x-axis. One row per shot, the columns designate the BPM channels.
        self.ymat = y_Matrix                #A numpy array matrix containing the data points on the y-axis. One row per shot, the columns designate the BPM channels.
        self.pos = z_Position               #Numpy array of floats containing the z positions of the BPM channels
        self.line = axis                    #Either 'Aramis' or 'Athos'
        self.chn_labels = channel_labels    #Numpy array of strings containing the channel names ordered by z position
        self.label = lab                    #A string containig date and time of the sample to be used in the names of saved plots

    def amount_shots(self):
        #Outputs the amount of samples
        return len(self.xmat[:,0])

    def amount_channels(self):
        #Outputs the number of recorded channels
        return len(self.xmat[0])

    def mean(self):
        #Outputs the mean per channel. The 0th entry in the numpy array are the x-axis channels, while the first entry are the y-axis channels
        means_per_channel_x = np.mean(self.xmat, axis=0)
        means_per_channel_y = np.mean(self.ymat, axis=0)
        return np.array([means_per_channel_x, means_per_channel_y])

    def rms(self):
        #Outputs the rms per channel. The 0th entry in the numpy array are the x-axis channels, while the first entry are the y-axis channels
        rms_x = np.std(self.xmat, axis=0, ddof=1)
        rms_y = np.std(self.ymat, axis=0, ddof=1)
        return np.array([rms_x, rms_y])

    def display_rms(self,output_channels = 'all', save=False):
        '''
        Generates one plot containing the rms development on the x- and y-axis
        :param output_channels: Option to limit the amount of channels shown in the plot. If output_channels is set to an integer, the rms gets displayed only up to that point
        :param save: Set save to True if the generated plot should the saved to the current directory
        :return:-
        '''
        factor = 10**3
        rms = self.rms()
        plt.figure()
        if output_channels == 'all':
            plt.plot(self.pos, rms[0]*factor, color='b', label='X')
            plt.plot(self.pos, rms[1]*factor, color='r', label='Y')
        else:
            plt.plot(self.pos[:output_channels], rms[0,:output_channels]*factor, color='b', label='X')
            plt.plot(self.pos[:output_channels], rms[1,:output_channels]*factor, color='r', label='Y')
        plt.xlabel('Z Position [m]')
        plt.ylabel('$\sigma_{x,y}$ [$\mu$m]')
        plt.grid()
        plt.title(self.line + ': RMS Development')
        plt.legend()
        if save:
            plt.savefig('RMSDevelopment'+'_'+self.label+'_'+self.line+'.pdf')
        plt.show()

    def freq_spec(self,axis,chn):
        '''
        :param axis: Either 'X' or 'Y'. Designates axis, from which the fourier spectrum is to be generated.
        :param chn: Integer of the channel in the channel list, from which the data is to be evaluated.
        :return: Fourier spectrum up to 50Hz
        '''
        if axis == 'X':
            data=self.xmat[:,chn]
        elif axis == 'Y':
            data=self.ymat[:,chn]
        else:
            print('Invalid Axis Input')
            return
        fourier = np.abs(np.fft.rfft(data))
        n = len(fourier)
        spec = fourier/n
        spec[0] = 0
        return spec

    def get_frequencies(self):
        #Outputs the frequencies corresponding to the Fourier amplitudes generated in freq_spec
        delta_t = 0.01#s
        freq = np.fft.rfftfreq(self.amount_shots(), d=delta_t)
        return freq

    def freq_development(self,axis,freq):
        '''
        Outputs the Fourier amplitude of one particular frequency along the z axis (along the beam propagation)
        :param axis: Either 'X' or 'Y'. Designates axis, from which the fourier spectrum is to be generated.
        :param freq: Int/Float of the frequency, with which the analysis should be done
        :return: Numpy array of Fourier amplitudes along the z-axis
        '''
        frequencies = self.get_frequencies()
        index = 'none'
        for i, f in enumerate(frequencies):
            if np.abs(f-freq)<0.05:
                index = i
            else:
                continue
        if index=='none':
            print('Frequency not available')
            return
        res = np.zeros(self.amount_channels())
        for i in range(0,self.amount_channels()):
            res[i] = self.freq_spec(axis, i)[index]
        return res

    def freq_norm_factors(self, axis):
        '''
        Computes the total absolute frequency amplitude per channel by summing up the squared length of all the amplitudes of one channel and then squaring the resulting term
        :param axis: Takes in 'X' or 'Y' for x- and y-axis respectively.
        :return: Outputs a numpy array with the frequency amplitude normalisation factors.
        '''
        factors = np.zeros(self.amount_channels())
        for i in range(0, self.amount_channels()):
            spec = self.freq_spec(axis, i)
            factors[i] = np.sqrt(np.sum(spec**2))
        return factors

    def display_freq_development(self, axis, freq, save=False):
        '''
        Generates a plot of the development of the rms normalised Fourier amplitude of the input frequency along the z-axis
        :param axis: Either 'X' or 'Y'. Designates axis, from which the fourier spectrum is to be generated.
        :param freq: Int/Float of the frequency, with which the analysis should be done
        :param save: Boolean to indicate, whether the plot should be saved (True) or not (False) to the current directory
        :return: -
        '''
        frequencies = self.freq_development(axis, freq)
        if axis=='X':
            c = 'b'
            index = 0
        else:
            c = 'r'
            index = 1
        plt.figure()
        plt.plot(self.pos, frequencies/self.freq_norm_factors(axis), color=c, label=axis+'-Axis')
        plt.title(self.line+': Development of the ' + str(freq)+'$Hz$ Frequency on the ' + axis + '-Axis')
        plt.grid()
        plt.xlabel('Z Position [m]')
        plt.ylabel('Normalized Frequency Amplitude')
        if save:
            plt.savefig('FreqDev_'+str(freq)+'_'+self.label+'_'+self.line+'_'+axis+'.pdf')
        plt.show()

    def odd_even_output(self, chn):
        '''
        Sorts every other shot of the specified channel into one of two numpy arrays (even or odd). The designation is arbitrary
        :param chn: Channel, for which the sorting should be done. Input can either be a string (name of a specific channel) or the index of a channel on the channel list.
        :return: 'Even' numpy array containing a numpy array with the x- and a numpy array with the y-axis values, 'Odd' numpy array containing a numpy array with the x- and a numpy array with the y-axis values, integer index of the input channel
        '''
        if type(chn) == str:
            for k, c in enumerate(self.chn_labels):
                if chn == c:
                    digit = k
                else:
                    digit = self.amount_channels() - 1
        else:
            digit = chn
        mean = self.mean()
        centered_x = self.xmat[:,digit]-mean[0,digit]
        centered_y = self.ymat[:,digit]-mean[1,digit]
        n = self.amount_shots()
        if n % 2 == 0:
            index = int((n+1)/2)
        else:
            index = int(n/2)
        x_odd = np.zeros(int(n/2))
        y_odd = np.zeros(int(n/2))
        x_even = np.zeros(index)
        y_even = np.zeros(index)
        if n % 2 == 0:
            index -= 1
        for i in range(0,index):
            x_even[i] = centered_x[2*i]
            y_even[i] = centered_y[2*i]
            x_odd[i] = centered_x[2*i+1]
            y_odd[i] = centered_y[2*i+1]
        if n % 2 == 0:
            x_even[-1] = centered_x[-1]
            y_even[-1] = centered_y[-1]
        return np.array([x_even, y_even]), np.array([x_odd, y_odd]), digit

    def display_odd_even_distribution(self, chn, save=False):
        '''
        Generates an even-odd distribution slice (scatter plot) of the desired channel
        :param chn: Channel, from which the plot should be generated. Input can either be a string (channel name) or the index integer from the channel list
        :param save: Boolean to indicate, whether the plot should be saved (True) or not (False) to the current directory
        :return:
        '''
        factor = 10**3
        even, odd, channel_ind = self.odd_even_output(chn)
        plt.figure()
        plt.scatter(even[0]*factor, even[1]*factor, s=4, color='g', label='Even Shots')
        plt.scatter(odd[0]*factor, odd[1]*factor, s=4, color='darkorange', label='Odd Shots')
        plt.legend()
        plt.xlabel('X-Axis [$\mu$m]')
        plt.ylabel('Y-Axis [$\mu$m]')
        plt.title(self.line +': Even Odd Distribution for Channel '+ self.chn_labels[channel_ind])
        plt.grid()
        plt.axis('equal')
        if save:
            plt.savefig('EvenOddDist_'+self.label+'_'+self.line+'_'+self.chn_labels[channel_ind]+'.pdf')
        plt.show()
