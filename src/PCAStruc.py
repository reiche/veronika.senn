import numpy as np
import matplotlib.pyplot as plt

'''
    PCA (Prinipal Component Analysis) class initialised by inputting an object of class BPMData. 
    Class functions can be grouped in data information and 2 basic analysis types: Relative Magnitudes and ev detracted rms.     
        Data Information:       ev(index)
        Relative Magnitudes:    relative_magnitudes()
        EV detracted RMS:       ev_detracted_data_collection(resolution, auto=False)
                                compute_projection(EV_Index)    (intermediary function)
                                ev_detraction_step(EV_Index, input_matrix, axis='None')   (intermediary function)
                                
    Class functions to generate single plots:   display_EV(EV_index=1, save=False)
                                                display_rel_magnitudes(threshold=1.0, save=False)
                                                display_rms_plot(resolution, axis, save=False)
                                
'''


class PCA:
    def __init__(self, Data_Mat):
        self.no_channels = Data_Mat.amount_channels()                                       #An integer that gives the number of channels evaluated for this data set
        self.no_shots = Data_Mat.amount_shots()                                             #An integer that gives the number of shots evaluated for this data set
        self.label = Data_Mat.label                                                         #A string containig date and time of the sample to be used in the names of saved plots
        self.line = Data_Mat.line                                                           #Either 'Aramis' or 'Athos'
        self.pos = Data_Mat.pos                                                             #Numpy array of floats containing the z positions of the BPM channels
        self.centered_data = np.zeros((self.no_shots, self.no_channels*2))                  #self.centered_data:    A numpy array matrix containing the data points from the x as well as y axis (dimension: amount shots X 2*amount channnels)
        self.centered_data[:,:self.no_channels] = Data_Mat.xmat - Data_Mat.mean()[0]        #                       The mean per channel has been detracted from the data points.
        self.centered_data[:,self.no_channels:] = Data_Mat.ymat - Data_Mat.mean()[1]
        _, self.sigma, self.Vh = np.linalg.svd(self.centered_data)                          #self.sigma:    Array containing the singular values of the SVD in the descending order
                                                                                            #self.Vh:       Matrix containing the unitary arrays from the SVD (assorted in rows). In SVD, Vh has the dimensions no_channelsXno_channels
    def ev(self, index):
        #Outputs the index-th eigenvector produced by the SVD. (Indices start with 1 and go up to n=amount channels)
        return self.Vh[index-1,:]

    def display_EV(self, EV_index=1, save=False):
        '''
        Generates a plot of the index-th eigenvector for the x- as well as the y-axis
        :param EV_index: Integer of the eigenvector (Indices start with 1 and go up to n=amount channels)
        :param save: Boolean to indicate, whether the plot should be saved (True) or not (False) to the current directory
        :return: -
        '''
        ev = self.ev(EV_index)
        factor = 10**3
        plt.figure()
        plt.plot(self.pos, ev[:self.no_channels]*factor, color='b', label='X')
        plt.plot(self.pos, ev[self.no_channels:]*factor, color='r', label='Y')
        plt.xlabel('Z Position [$m$]')
        plt.ylabel('Displacement from Data Mean [$\mu$m]')
        plt.title(self.line+': '+str(EV_index)+'. Principal Component')
        plt.legend()
        plt.grid()
        if save:
            plt.savefig('PCA'+str(EV_index)+'EV_'+self.label+'_'+self.line+'.pdf')
        plt.show()

    def relative_magnitudes(self):
        #Outputs a numpy array of the relative magnitudes of the singular values from the SVD in percent. Magnitudes measured in the squared L^2 norm.
        tot_lambda = np.sum((self.sigma)**2)
        rel_mag = self.sigma**2/tot_lambda*100
        return rel_mag

    def display_rel_magnitudes(self, threshold=1.0, save=False):
        '''
        Generates a bar plot for the relative magnitudes of the singular values from the SVD.
        :param threshold: Threshold as to what magnitude of SV should be displayed. Default set to 1 percent (equal or lower will not be displayed).
        :param save: Boolean to indicate, whether the plot should be saved (True) or not (False) to the current directory
        :return: -
        '''
        t_index = 0
        rel_mag = self.relative_magnitudes()
        for n, l in enumerate(rel_mag):
            if l < threshold:
                t_index = n
                break
        labels = [str(k+1)+'.' for k in range(0,t_index)]
        plt.figure()
        plt.bar(labels, rel_mag[:t_index])
        plt.xlabel('Components Ordered by Magnitude')
        plt.ylabel('Contribution to Total [%]')
        plt.title(self.line+': Relative Magnitudes of the First ' +str(t_index)+' Components')
        for i in range(len(labels)):
            plt.text(i, rel_mag[i], round(rel_mag[i],1), ha ='center')
        plt.grid(axis='y')
        if save:
            plt.savefig('RelativeMagnitudesEV_'+self.label+'.pdf')
        plt.show()

    def compute_projection(self, EV_Index):
        #Outputs the projection vector from the eigenvector (specified by the EV_Index) and the centered data matrix. (Multiplication of the ev with a shot row in the data matrix gives the projection scalar for this particular data row)
        projection_1D = np.array([self.centered_data.dot(self.Vh.T[:,EV_Index])])
        return np.matmul(projection_1D.T, [self.Vh[EV_Index,:]])

    def ev_detraction_step(self, EV_Index, input_matrix, axis='None'):
        #Outputs the centred data adjusted by the detraction of the specified ev projection
        if axis == 'None':
            return input_matrix - self.compute_projection(EV_Index)
        elif axis == 'x':
            return input_matrix - self.compute_projection(EV_Index)[:,:self.no_channels]
        elif axis == 'y':
            return input_matrix - self.compute_projection(EV_Index)[:,self.no_channels:]
        else:
            print('Invalid Axis Input')
            return

    def ev_detracted_data_collection(self, resolution, auto=False):
        '''
        Collects each step of the centered data set adjusted by the detraction of ev projections.
        :param resolution: maximal number of eigenvectors to be detracted from the rms (sequentially).
        :param auto: Boolean parameter to determine, if the data is processed for the function 'display_rms_plot'
        :return: Numpy array matrix of dimensions (resolution X amount shots X 2*amount channels) containing the centred data sequentially adjusted by the detraction of ev projections.
        '''
        matrix_of_each_step = np.zeros((resolution, self.no_shots, 2*self.no_channels+1))
        matrix_of_each_step[0,:,:-1] = self.ev_detraction_step(0,self.centered_data)
        for k in range(1,resolution):
            matrix_of_each_step[k,:,:-1] = self.ev_detraction_step(k, matrix_of_each_step[k-1,:,:-1])
        if auto:
            return matrix_of_each_step
        else:
            return matrix_of_each_step[:,:,:-1]

    def display_rms_plot(self, resolution, axis, save=False):
        '''
        Generates a plot of the ev detracted rms of either the x- or the y-axis
        :param resolution: maximal number of eigenvectors to be detracted from the rms (sequentially).
        :param axis: Either 'X' or 'Y'. Designates axis, from which the plot is to be generated.
        :param save: Boolean to indicate, whether the plot should be saved (True) or not (False) to the current directory
        :return:
        '''
        factor = 10**3
        matrix = self.ev_detracted_data_collection(resolution, auto=True)
        if axis == 'X':
            start = 0
            end = self.no_channels
            original_rms = np.std(self.centered_data[:,:end], axis=0, ddof=1)
        elif axis == 'Y':
            start = self.no_channels
            end = -1
            original_rms = np.std(self.centered_data[:,start:], axis=0, ddof=1)
        else:
            print('Invalid Axis Input')
            return
        plt.figure()
        plt.plot(self.pos, original_rms*factor, label='Original')
        for k in range(0, resolution):
             rms = np.std(matrix[k,:,start:end], axis=0,ddof=1)
             plt.plot(self.pos, rms*factor, label=str(k+1)+'. Component')
        plt.title(self.line+ ': Fluctuation in '+axis+'-Axis')
        plt.xlabel('Z Position [m]')
        plt.ylabel('$\sigma_'+axis+'$ [$\mu$m]')
        plt.grid()
        plt.legend()
        if save:
            plt.savefig('RMSReductionByEV_'+self.label+'_'+self.line+'_'+axis+'.pdf')
        plt.show()
