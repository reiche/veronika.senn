import numpy as np
from bsread import source
from datetime import datetime
from os import getlogin
import elog
from src.DataStruc import BPMData
from src.HDF5filingStruc import HDF5_file
from src.AssembleChannelList import AssembleList

'''
    Functions to collect current BPM data. 
    Main function: generate_data(local_file_address, shots='5000')
'''

def Row_of_one_Pulse(dictionary, channellist):
    #Function to produce one shot row from the current stream loop
    length = len(channellist)
    row = np.zeros(length)
    for n, channel in enumerate(channellist):
        row[n] = dictionary.data.data[channel].value
    return row

def LogbookEntry(amount, fileaddress, author):
    #Function to produce a logbook entry for the completed measurement
    attachement = None
    category = 'Measurement'
    system = 'Beamdynamics'
    title = 'BPM Data'
    application = 'Jitter Analyis'
    text = 'BPM data of a total of ' + str(amount) + ' shots. \n Data saved at: ' + fileaddress
    dict_att = {'Author':author, 'Application':application, 'Category':category, 'Title':title, 'System':system}
    print('\nLog book entry generated')
    logbook = elog.open('https://elog-gfa.psi.ch/SwissFEL+commissioning+data/', user='robot', password='robot')
    return logbook.post(text, attributes=dict_att, attachments=attachement)



def generate_data(local_file_address, shots='5000'):
    '''
    Function to access the stream to create beam synchronous data and the structured data to a HDF5 file
    :param local_file_address: path to the user's desired directory to save the data to
    :param shots: Amount of shots for which the data shall be generated.
    :return: HDF5_file object corresponding to the file to which the data has been saved
    '''
    amount_samples = int(shots)

    #Collect the channel lists
    axis_1, axis_2, pos1, pos2 = AssembleList()

    #Iniate array for pulse id and create the current time stamp
    id_labels = np.zeros(amount_samples)
    t = datetime.now()

    #Initialise BPMData objects with the data matrices of the appropriate sizes
    DataMat1 = BPMData(x_Matrix=np.zeros((amount_samples, len(axis_1[0]))), y_Matrix=np.zeros((amount_samples, len(axis_1[1]))), z_Position=pos1, axis='Aramis')
    DataMat2 = BPMData(x_Matrix=np.zeros((amount_samples, len(axis_2[0]))), y_Matrix=np.zeros((amount_samples, len(axis_2[1]))), z_Position=pos2, axis='Athos')

    #Collectively input all the BPM channels and loop through the stream until the desired amount of shots has been reached.
    entire_chlst = np.concatenate((axis_1[0], axis_1[1], axis_2[0], axis_2[1]))
    count = 0
    with source(channels = entire_chlst) as stream:
        while count<amount_samples:
            msg = stream.receive()
            skip=False
            for chn in entire_chlst:
                if msg.data.data[chn].value is None:
                    skip=True
            if skip or msg.data.data[entire_chlst[0]].value == 0:
                continue
            else:
                DataMat1.xmat[count] = Row_of_one_Pulse(msg, axis_1[0])
                DataMat1.ymat[count] = Row_of_one_Pulse(msg, axis_1[1])
                DataMat2.xmat[count] = Row_of_one_Pulse(msg, axis_2[0])
                DataMat2.ymat[count] = Row_of_one_Pulse(msg, axis_2[1])
                id_labels[count] = msg.data.pulse_id
                count += 1
                continue

    #Create numpy arrays containing the channel names for each line
    DataMat1.chn_labels = np.array([channel[:15] for channel in axis_1[0]])
    DataMat2.chn_labels = np.array([channel[:15] for channel in axis_2[0]])

    #Define the name of the file and the path to directory it is saved to
    path = '/sf/data/measurements/'+ t.strftime('%Y')+'/'+t.strftime('%m')+'/'+t.strftime('%d')+'/'
    filename = 'BPMReadouts_' + t.strftime('%Y')+t.strftime('%m')+t.strftime('%d')+'_'+t.strftime('%H')+t.strftime('%M')+t.strftime('%S')+'.h5'

    #Initialize two HDF5_file objects to save the data once to the current PSI measurement directory and once to the user's directory
    H5_file1 = HDF5_file(filename, local_file_address)
    H5_file1.save_BPMData(DataMat1, DataMat2, labels=id_labels)
    H5_file2 = HDF5_file(filename, path)
    H5_file2.save_BPMData(DataMat1, DataMat2, id_labels)

    #Create Logbook entry
    _ = LogbookEntry(amount_samples, path+filename, author=getlogin())

    print('\nData collected for ' + str(DataMat1.amount_channels()) + ' channels on first axis and ' + str(DataMat2.amount_channels()) + ' channels on second axis\n')

    return H5_file1
